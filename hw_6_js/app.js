class UserInfo{
    constructor(continent,country,regionName,city,district){
        this.continent = continent;
        this.country = country;
        this.regionName = regionName;
        this.city = city;
        this.district = district;
    }
    createElement(elemType, classNames, text) {
        const element = document.createElement(elemType);
        if (text) {
          element.textContent = text;
        }
        element.classList.add(...classNames);
        return element;
      }

    render(){
        this.info = this.createElement("div", ["user-info"]);
        this.info.insertAdjacentHTML("afterbegin", `
        <p>Continent: ${this.continent}</p>
        <p>Country: ${this.country}</p>
        <p>Region: ${this.regionName}</p>
        <p>City: ${this.city}</p>
        <p>District: ${this.district}</p>
        `);
        return this.info;
    }
}


function checkIp(){
    const btn = document.querySelector('#btn-ip');
    btn.addEventListener('click', async ()=>{
        const getIp = await fetch(`https://api.ipify.org/?format=json`)
        const clientIp = await getIp.json();
        const getInfo = await fetch(`http://ip-api.com/json/${clientIp.ip}?fields=status,continent,country,regionName,city,district`)
        const clientInfo = await getInfo.json()
        const {continent,country,regionName,city,district} = clientInfo;
        const info = new UserInfo(continent,country,regionName,city,district)
        btn.after(info.render())
    })
}

checkIp()