const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];
let allKey = ['author', 'name', 'price'];
const ul = document.createElement('ul');
document.querySelector('#root').append(ul);

function checkBook(obj, index){
    for(let key = 0; key < allKey.length; key++){
        try{
            if(!obj.hasOwnProperty(allKey[key])){
                throw new Error(`The book:${index+1} does not contain a field '${allKey[key]}'`)
            } 
        }
        catch(e){
            console.log(e.message);
        }
    }
};
books.forEach((item,index) => {
    checkBook(item,index)
    if(allKey.every(key => Object.keys(item).includes(key))){
        Object.entries(item).map(book => {
            const [key,value] = book;
            ul.innerHTML += `<li>${key} : ${value}</li>`
        })
    }   
})




