class Film {
    constructor(episodeId, name, openingCrawl,characters){
        this.episodeId = episodeId;
        this.name = name;
        this.openingCrawl = openingCrawl;
        this.characters = characters;
    }
    createElement(elemType, classNames, text) {
        const element = document.createElement(elemType);
        if (text) { element.textContent = text; }
        element.classList.add(...classNames);
        return element
    }
    getCharacters(){
        this.allCharacters = this.createElement("div",["loader"])
        this.ol = this.createElement("ol",["characters"]);
        this.loader = this.createElement("div", ["lds-spinner"]);
        this.loader.insertAdjacentHTML('afterbegin', `<div></div><div></div><div></div><div></div><div>
        </div><div></div><div></div><div></div><div></div><div></div><div></div><div></div>`);
        if (!this.loader.classList.contains('active')) {
            this.loader.style.display = 'inline-block';
            this.loader.classList.add('active');
        }
        Promise.all(this.characters.map(url =>
            fetch(url).then(resp => resp.json())
        ))
        .then(data => {
            this.loader.style.display = 'none';
            data.map(item => {
                const {name} = item;
                this.ol.insertAdjacentHTML('afterbegin',`<li>${name}</li>`)                
            })
        });
        this.allCharacters.append(this.loader, this.ol)
        return this.allCharacters       
    }
    renderFilms(){
        this.body = this.createElement("div",["film"]);
        this.body.insertAdjacentHTML('afterbegin', 
        `<p>Episode : ${this.episodeId}</p>
        <p>Film name: "${this.name}"</p>
        <p>${this.openingCrawl}</p>`);
        this.body.append(this.getCharacters());   
        return this.body
    }
};
function displayFilms(){
    const root = document.querySelector('#root');
    fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(response => response.json())
    .then(data => data
        .sort(({episodeId: episodeA}, {episodeId: episodeB}) => episodeA - episodeB)
        .forEach(item => {
        const {episodeId,name,openingCrawl,characters} = item;
        const film = new Film(episodeId,name,openingCrawl,characters)
        root.append(film.renderFilms());
    }))  
}
displayFilms()

