class Employee{
    constructor(options){
        this._name = options.name;
        this._age = options.age;
        this._salary = options.salary;
    };
    get name(){
        return this._name;
    };
    set name(newName){
        this._name = newName
    };
    get age(){
        return this._age;
    };
    set age(newAge){
        this._age = newAge
    };
    get salary(){
        return this._salary;
    };
    set salary(newSalary){
        this._salary = newSalary
    };
};
class Programmer extends Employee{
    constructor(options){
        super(options)
        this._lang = options.lang
    }
    get lang(){
        return this._lang
    };
    set lang(newLang){
        this._lang = newLang
    };
    get salary(){
        return this._salary * 3;
    };
};
const employee = new Employee({
    name: 'John',
    age: 23,
    salary: 1500
});
const userMike = new Programmer({
    name: 'Mike',
    age: 27,
    salary: 1700,
    lang: 'ukrainian'
});
const userSteve = new Programmer({
    name: 'Steve',
    age: 37,
    salary: 1900,
    lang: 'american'
});
const userSara = new Programmer({
    name: 'Sara',
    age: 46,
    salary: 2700,
    lang: 'canadian'
});

console.log(`Name: ${employee.name}, age: ${employee.age}, salary: ${employee.salary}`);
console.log(`Name: ${userMike.name}, age: ${userMike.age}, salary: ${userMike.salary}, language: ${userMike.lang}`);
console.log(`Name: ${userSteve.name}, age: ${userSteve.age}, salary: ${userSteve.salary}, language: ${userSteve.lang}`);
console.log(`Name: ${userSara.name}, age: ${userSara.age}, salary: ${userSara.salary}, language: ${userSara.lang}`);
userSara.lang = 'american';
console.log(`Name: ${userSara.name}, age: ${userSara.age}, salary: ${userSara.salary}, language: ${userSara.lang}`);

