class Card {
  constructor(userId, name, email, postId, title, body) {
    this.userId = userId;
    this.name = name;
    this.email = email;
    this.postId = postId;
    this.title = title;
    this.body = body;
  }
  createElement(elemType, classNames, text) {
    const element = document.createElement(elemType);
    if (text) {
      element.textContent = text;
    }
    element.classList.add(...classNames);
    return element;
  }
  attachListener() {
    const closeBtn = this.card.querySelector(".delete");
    closeBtn.addEventListener("click", () => {
      const toDelCard = confirm("Are you sure you want to delete this card?");
      if (toDelCard) {
        fetch(`https://ajax.test-danit.com/api/json/posts/${this.postId}`, {
          method: "DELETE",
        }).then((response) => {
          if (response.ok) {
            this.card.remove();
          }
        });
      }
    });
  }
  renderHeader() {
    this.cardHeader = this.createElement("div", ["cardHeader"]);
    this.cardHeader.insertAdjacentHTML(
      "afterbegin",
      `<span class="name">${this.name}</span>
            <span class="email">${this.email}</span>
            <span class="delete">X</span>
            `
    );
    return this.cardHeader;
  }
  renderBody() {
    this.cardBody = this.createElement("div", ["cardBody"]);
    this.cardBody.insertAdjacentHTML(
      "afterbegin",
      `<h3>${this.title}</h3>
            <p>${this.body}</p>`
    );
    return this.cardBody;
  }
  render() {
    this.card = this.createElement("div", ["userCard"]);
    this.card.append(this.renderHeader(), this.renderBody());
    this.attachListener();
    return this.card;
  }
}

const components = {
  root: document.querySelector("#root"),
  loader: document.querySelector(".lds-dual-ring"),
};

function getUser() {
  let urls = [];
  const newUsers = [];
  fetch("https://ajax.test-danit.com/api/json/users")
    .then((response) => response.json())
    .then((users) => {
      users.forEach((user) => {
        newUsers.push(user);
        urls.push(
          `https://ajax.test-danit.com/api/json/posts?userId=${user.id}`
        );
      });
      Promise.all(
        urls.map((url) => {
          return fetch(url).then((response) => response.json());
        })
      ).then((response) =>
        response.forEach((posts, index) => {
          const { name, email } = newUsers[index];
          posts.map((post) => {
            const { id, userId, title, body } = post;
            const card = new Card(userId, name, email, id, title, body);
            components.root.append(card.render());
          });
          components.loader.remove();
          components.root.style.display = "grid";
        })
      );
    });
}
getUser();
