class Card {
    constructor(userId, title, body){
        this.userId = userId;
        this.title = title;
        this.body = body;
    }
    createElement(elemType, classNames, text) {
        const element = document.createElement(elemType);
        if (text) { element.textContent = text; }
        element.classList.add(...classNames);
        return element
    }
    getUser(){
        this.cardHeader = this.createElement('div',['cardHeader']);
        fetch(`https://ajax.test-danit.com//api/json/users/${this.userId}`)
        .then(response => response.json())
        .then(data => {
            const {name,email} = data;
            this.cardHeader.insertAdjacentHTML('afterbegin', 
            `<p>${name}</p>
            <p>${email}</p>`)
        })
        return this.cardHeader
    }
    render(){
        this.card = this.createElement('div',['userCard']);
        this.cardBody = this.createElement('div',['cardBody']);
        this.cardBody.insertAdjacentHTML('afterbegin',
        `<h3>${this.title}</h3>
        <p>${this.body}</p>`);
        this.card.append(this.getUser(),this.cardBody);
        return this.card;
    }
}
const components = {
    root: document.querySelector('#root'),
    main: document.querySelector('.main'),
    loader: document.querySelector('.lds-dual-ring')
}
function getPosts(){
    fetch('https://ajax.test-danit.com/api/json/posts')
    .then(response => response.json())
    .then(data => data
        .forEach(item => {
         const {userId, title, body} = item;
         const card = new Card(userId,title,body);
         components.root.append(card.render())
        }))       
}
function load(){
    setTimeout(()=>{
        components.loader.remove()
        components.main.style.display = 'block'
    },2000)
}
getPosts();
load()

// fetch('https://ajax.test-danit.com//api/json/posts?userId=1')
//     .then(response => response.json())
//     .then(data => console.log(data))

// fetch(`https://ajax.test-danit.com/api/json/posts?userId=${this.userId}`)